const http = require("http");

const port = 4000;

const server = http.createServer((request, response) => {
	//first endpoint is /greeting --> "http://localhost:4000/greeting"
	if (request.url == "/greeting"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Hello Again!");
	}else if (request.url == "/homepage"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("This is the Home Page!");
	}else{
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("ERROR: 404 - PAGE NOT FOUND");
	}
});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);